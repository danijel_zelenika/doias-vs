#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*--------------------------------------------------------------------------*/
/*                                                                          */
/*                       DISCRETE COSINE TRANSFORM                          */
/*                                                                          */
/*          (Copyright Joachim Weickert and Andres Bruhn, 11/2007)          */
/*                                                                          */
/*--------------------------------------------------------------------------*/


/*--------------------------------------------------------------------------*/

void alloc_matrix

(float ***matrix,  /* matrix */
	long  nx,         /* size in x-direction */
	long  ny)         /* size in y-direction */

					  /* allocates storage for matrix of size nx * ny */


{
	long i;

	*matrix = (float **)malloc(nx * sizeof(float *));
	if (*matrix == NULL)
	{
		printf("alloc_matrix: not enough storage available\n");
		exit(1);
	}
	for (i = 0; i < nx; i++)
	{
		(*matrix)[i] = (float *)malloc(ny * sizeof(float));
		if ((*matrix)[i] == NULL)
		{
			printf("alloc_matrix: not enough storage available\n");
			exit(1);
		}
	}
	return;
}

/*--------------------------------------------------------------------------*/

void disalloc_matrix

(float **matrix,   /* matrix */
	long  nx,         /* size in x-direction */
	long  ny)         /* size in y-direction */

					  /* disallocates storage for matrix of size nx * ny */

{
	long i;
	for (i = 0; i < nx; i++)
		free(matrix[i]);
	free(matrix);
	return;
}

/*--------------------------------------------------------------------------*/

void analyse_RGB

(float  **R,          /* Red channel of RGB image */
	float  **G,          /* Green channel of RGB image */
	float  **B,          /* Blue channel of RGB image */
	long    nx,          /* pixel number in x-direction */
	long    ny,          /* pixel number in y-direction */
	float   *min,        /* minimum, output */
	float   *max,        /* maximum, output */
	float   *mean,       /* mean, output */
	float   *vari)       /* variance, output */

						 /*
						 calculates minimum, maximum, mean and variance of a RGB image
						 */

{
	long    i, j;       /* loop variables */
	float   help;       /* auxiliary variable */
	double  help2;      /* auxiliary variable */

	*min = R[1][1];
	*max = R[1][1];

	help2 = 0.0;

	for (i = 1; i <= nx; i++)
		for (j = 1; j <= ny; j++)
		{
			if (R[i][j] < *min) *min = R[i][j];
			if (R[i][j] > *max) *max = R[i][j];
			help2 = help2 + (double)R[i][j];
			if (G[i][j] < *min) *min = G[i][j];
			if (G[i][j] > *max) *max = G[i][j];
			help2 = help2 + (double)G[i][j];
			if (B[i][j] < *min) *min = B[i][j];
			if (B[i][j] > *max) *max = B[i][j];
			help2 = help2 + (double)B[i][j];
		}
	*mean = (float)help2 / (nx * ny * 3);

	*vari = 0.0;
	for (i = 1; i <= nx; i++)
		for (j = 1; j <= ny; j++)
		{
			help = R[i][j] - *mean;
			*vari = *vari + help * help;
			help = G[i][j] - *mean;
			*vari = *vari + help * help;
			help = B[i][j] - *mean;
			*vari = *vari + help * help;
		}
	*vari = *vari / (nx * ny * 3);

	return;

} /* analyse */


  /*--------------------------------------------------------------------------*/

void RGB_to_YCbCr

(float  **R,          /* Red channel of RGB image */
	float  **G,          /* Green channel of RGB image */
	float  **B,          /* Blue channel of RGB image */
	float  **Y,          /* Y channel of YCbCr image */
	float  **Cb,         /* Cb channel of YCbCr image */
	float  **Cr,         /* Cr channel of YCbCr image */
	long    nx,          /* pixel number in x-direction */
	long    ny)          /* pixel number in y-direction */

						 /*
						 converts RGB to YCbCr colour space
						 */

{
	long    i, j;        /* loop variables */
	float   help;        /* time saver */

	help = 1.0 / 256.0;

	/* sets higher frequencies to zero */
	for (i = 1; i <= nx; i++)
		for (j = 1; j <= ny; j++)
		{
			/*
			SUPPLEMENT CODE HERE
			*/
		}
	return;
}


/*--------------------------------------------------------------------------*/

void YCbCr_to_RGB

(float  **Y,          /* Y channel of YCbCr image */
	float  **Cb,         /* Cb channel of YCbCr image */
	float  **Cr,         /* Cr channel of YCbCr image */
	float  **R,          /* Red channel of RGB image */
	float  **G,          /* Green channel of RGB image */
	float  **B,          /* Blue channel of RGB image */
	long    nx,          /* pixel number in x-direction */
	long    ny)          /* pixel number in y-direction */

						 /*
						 converts YCbCr to RGB colour space
						 */

{
	long    i, j;        /* loop variables */


						 /* sets higher frequencies to zero */
	for (i = 1; i <= nx; i++)
		for (j = 1; j <= ny; j++)
		{
			R[i][j] = 1.159 * (Y[i][j] - 16)
				- 0.009 * (Cb[i][j] - 128)
				+ 1.596 * (Cr[i][j] - 128);

			G[i][j] = 1.159 * (Y[i][j] - 16)
				- 0.401 * (Cb[i][j] - 128)
				- 0.813 * (Cr[i][j] - 128);

			B[i][j] = 1.159 * (Y[i][j] - 16)
				+ 2.008 * (Cb[i][j] - 128)
				+ 0.000 * (Cr[i][j] - 128);
		}
	return;
}


/*--------------------------------------------------------------------------*/

void subsample_channel

(float  **c,          /* image channel, changed */
	long    nx,          /* pixel number in x-direction */
	long    ny,          /* pixel number in y-direction */
	long    S)           /* subsample factor */


						 /*
						 reduce resolution by averaging blocks of SxS neighbouring pixels
						 */

{
	long    i, j;        /* loop variables */
	long    k, l;        /* loop variables */
	float   sum;         /* summation variable */


						 /* replace SxS block by block average */
	for (i = 1; i <= nx; i += S)
		for (j = 1; j <= ny; j += S)
		{
			/* initialise sum */
			sum = 0.0;

			/* compute block average */
			for (k = 0; k < S; k++)
				for (l = 0; l < S; l++)
				{
					sum += c[i + k][j + l];
				}

			sum = sum / (S*S);

			/* set all block entries to average */
			for (k = 0; k < S; k++)
				for (l = 0; l < S; l++)
				{
					c[i + k][j + l] = sum;
				}
		}
	return;
}


/*--------------------------------------------------------------------------*/

int main()

{
	char   row[80];              /* for reading data */
	char   in[80];               /* for reading data */
	char   out[80];              /* for writing data */
	float  **R;                  /* Red channel of RGB image */
	float  **G;                  /* Green channel of RGB image */
	float  **B;                  /* Blue channel of RGB image */
	float  **Y;                  /* Y channel of YCbCr image */
	float  **Cb;                 /* Cb channel of YCbCr image */
	float  **Cr;                 /* Cr channel of YCbCr image */
	long   i, j;                 /* loop variables */
	long   nx, ny;               /* image size in x, y direction */
	long   S;                    /* subsampling factor */
	FILE   *inimage, *outimage;  /* input file, output file */
	float  sigma;                /* standard deviation */
	float  max, min;             /* largest, smallest grey value */
	float  mean;                 /* average grey value */
	float  vari;                 /* variance */
	unsigned char byte;          /* for data conversion */


								 /* ---- print out information ---- */

	printf("\n");
	printf("RGB TO YCBCR CONVERSION\n\n");
	printf("*************************************************\n");
	printf("\n");
	printf("    Copyright 2007 by Joachim Weickert\n");
	printf("    Faculty of Mathematics and Computer Science\n");
	printf("    Saarland University, Germany\n");
	printf("\n");
	printf("    All rights reserved. Unauthorized usage,\n");
	printf("    copying, hiring, and selling prohibited.\n");
	printf("\n");
	printf("    Send bug reports to\n");
	printf("    weickert@mia.uni-saarland.de\n");
	printf("\n");
	printf("*************************************************\n\n");


	/* ---- read input image (pgm format P6) ---- */

	/* read image name */
	printf("input image:                      ");
	gets(in);

	/* open pgm file and read header */
	inimage = fopen(in, "rb");
	fgets(row, 80, inimage);
	fgets(row, 80, inimage);
	while (row[0] == '#') fgets(row, 80, inimage);
	sscanf(row, "%ld %ld", &nx, &ny);
	fgets(row, 80, inimage);

	/* allocate storage */
	alloc_matrix(&R, nx + 2, ny + 2);
	alloc_matrix(&G, nx + 2, ny + 2);
	alloc_matrix(&B, nx + 2, ny + 2);
	alloc_matrix(&Y, nx + 2, ny + 2);
	alloc_matrix(&Cb, nx + 2, ny + 2);
	alloc_matrix(&Cr, nx + 2, ny + 2);

	/* read image data */
	for (j = 1; j <= ny; j++)
		for (i = 1; i <= nx; i++)
		{
			R[i][j] = (float)getc(inimage);
			G[i][j] = (float)getc(inimage);
			B[i][j] = (float)getc(inimage);
		}
	fclose(inimage);


	/* ---- analyse image ---- */

	analyse_RGB(R, G, B, nx, ny, &min, &max, &mean, &vari);
	printf("\n");
	printf("minimum:       %8.2f \n", min);
	printf("maximum:       %8.2f \n", max);
	printf("mean:          %8.2f \n", mean);
	printf("std. dev.:     %8.2f \n\n", sqrt(vari));


	/* ---- read other parameters ---- */

	printf("subsampling factor:               ");
	gets(row);  sscanf(row, "%ld", &S);
	printf("output image:                     ");
	gets(out);
	printf("\n");


	/* ---- check if image can be downsampled by a factor of S ----*/
	if ((nx%S != 0) || (ny%S != 0))
	{
		printf("\n\n Image size does not allow downsampling by factor %d! \n\n", S);
		return(0);
	}


	/* ---- process image ---- */

	RGB_to_YCbCr(R, G, B, Y, Cb, Cr, nx, ny);
	subsample_channel(Y, nx, ny, S);
	subsample_channel(Cb, nx, ny, S);
	subsample_channel(Cr, nx, ny, S);
	YCbCr_to_RGB(Y, Cb, Cr, R, G, B, nx, ny);


	/* ---- analyse processed image ---- */

	analyse_RGB(R, G, B, nx, ny, &min, &max, &mean, &vari);
	printf("minimum:       %8.2f \n", min);
	printf("maximum:       %8.2f \n", max);
	printf("mean:          %8.2f \n", mean);
	printf("std. dev.:     %8.2f \n\n", sqrt(vari));


	/* ---- write output image (pgm format P6) ---- */

	/* open file and write header (incl. filter parameters) */
	outimage = fopen(out, "wb");
	fprintf(outimage, "P6 \n");
	fprintf(outimage, "# Filtered Image\n");
	fprintf(outimage, "# initial image:  %s\n", in);
	fprintf(outimage, "# Cb Cr Subsampling factor: %ld \n", S);
	fprintf(outimage, "%ld %ld \n255\n", nx, ny);

	/* write image data and close file */
	for (j = 1; j <= ny; j++)
		for (i = 1; i <= nx; i++)
		{
			if (R[i][j] < 0.0)
				byte = (unsigned char)(0.0);
			else if (R[i][j] > 255.0)
				byte = (unsigned char)(255.0);
			else
				byte = (unsigned char)(R[i][j]);
			fwrite(&byte, sizeof(unsigned char), 1, outimage);

			if (G[i][j] < 0.0)
				byte = (unsigned char)(0.0);
			else if (G[i][j] > 255.0)
				byte = (unsigned char)(255.0);
			else
				byte = (unsigned char)(G[i][j]);
			fwrite(&byte, sizeof(unsigned char), 1, outimage);

			if (B[i][j] < 0.0)
				byte = (unsigned char)(0.0);
			else if (B[i][j] > 255.0)
				byte = (unsigned char)(255.0);
			else
				byte = (unsigned char)(B[i][j]);
			fwrite(&byte, sizeof(unsigned char), 1, outimage);
		}
	fclose(outimage);
	printf("output image %s successfully written\n\n", out);


	/* ---- disallocate storage ---- */

	disalloc_matrix(R, nx + 2, ny + 2);
	disalloc_matrix(G, nx + 2, ny + 2);
	disalloc_matrix(B, nx + 2, ny + 2);
	disalloc_matrix(Y, nx + 2, ny + 2);
	disalloc_matrix(Cb, nx + 2, ny + 2);
	disalloc_matrix(Cr, nx + 2, ny + 2);

	return(0);
}
