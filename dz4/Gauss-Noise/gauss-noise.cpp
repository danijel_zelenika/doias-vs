#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*--------------------------------------------------------------------------*/
/*                                                                          */
/*                     GAUSSIAN NOISE WITH ZERO MEAN                        */
/*                                                                          */
/*--------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/

void alloc_matrix

(float ***matrix,  /* matrix */
	long  nx,         /* size in x direction */
	long  ny)         /* size in y direction */

   /* allocates storage for matrix of size nx * ny */


{
	long i;

	*matrix = (float **)malloc(nx * sizeof(float *));
	if (*matrix == NULL)
	{
		printf("alloc_matrix: not enough storage available\n");
		exit(1);
	}
	for (i = 0; i < nx; i++)
	{
		(*matrix)[i] = (float *)malloc(ny * sizeof(float));
		if ((*matrix)[i] == NULL)
		{
			printf("alloc_matrix: not enough storage available\n");
			exit(1);
		}
	}
	return;
}

/*--------------------------------------------------------------------------*/

void disalloc_matrix

(float **matrix,   /* matrix */
	long  nx,         /* size in x direction */
	long  ny)         /* size in y direction */

   /* disallocates storage for matrix of size nx * ny */

{
	long i;
	for (i = 0; i < nx; i++)
		free(matrix[i]);
	free(matrix);
	return;
}

/*--------------------------------------------------------------------------*/

void gauss_noise

(float  **f,       /* image, changed */
	long   nx,        /* pixel number in x direction */
	long   ny,        /* pixel number in y direction */
	float  sigma)     /* standard deviation of Gaussian noise */

/*
 adds Gaussian noise with standard deviation sigma and mean 0 to
 the image f by means of the Box-Muller method.
*/

{
	long    i, j;        /* loop variables */
	float   pi;
	float   x, y, z;     /* random numbers */

	pi = 3.1415927;

	for (i = 1; i <= nx; i++)
		for (j = 1; j <= ny; j++)
		{
			/*
			 supplement the missing code here
			*/
		}

	return;
}

/*--------------------------------------------------------------------------*/

void analyse

(float   **u,         /* image, unchanged */
	long    nx,          /* pixel number in x direction */
	long    ny,          /* pixel number in x direction */
	float   *min,        /* minimum, output */
	float   *max,        /* maximum, output */
	float   *mean,       /* mean, output */
	float   *vari)       /* variance, output */

/*
 calculates minimum, maximum, mean and variance of an image u
*/

{
	long    i, j;       /* loop variables */
	float   help;       /* auxiliary variable */
	double  help2;      /* auxiliary variable */

	*min = u[1][1];
	*max = u[1][1];
	help2 = 0.0;
	for (i = 1; i <= nx; i++)
		for (j = 1; j <= ny; j++)
		{
			if (u[i][j] < *min) *min = u[i][j];
			if (u[i][j] > *max) *max = u[i][j];
			help2 = help2 + (double)u[i][j];
		}
	*mean = (float)help2 / (nx * ny);

	*vari = 0.0;
	for (i = 1; i <= nx; i++)
		for (j = 1; j <= ny; j++)
		{
			help = u[i][j] - *mean;
			*vari = *vari + help * help;
		}
	*vari = *vari / (nx * ny);

	return;

} /* analyse */

/*--------------------------------------------------------------------------*/

int main()

{
	char   row[80];              /* for reading data */
	char   in[80];               /* for reading data */
	char   out[80];              /* for reading data */
	float  **u;                  /* image */
	long   i, j;                 /* loop variables */
	long   nx, ny;               /* image size in x, y direction */
	FILE   *inimage, *outimage;  /* input file, output file */
	float  sigma;                /* standard deviation */
	float  max, min;             /* largest, smallest grey value */
	float  mean;                 /* average grey value */
	float  vari;                 /* variance */
	unsigned char byte;          /* for data conversion */

	printf("\n");
	printf("ADDING GAUSSIAN NOISE WITH ZERO MEAN\n\n");

	/* ---- read input image (pgm format P5) ---- */

	/* read image name */
	printf("input image:                      ");
	gets(in);

	/* open pgm file and read header */
	inimage = fopen(in, "rb");
	fgets(row, 80, inimage);
	fgets(row, 80, inimage);
	while (row[0] == '#') fgets(row, 80, inimage);
	sscanf(row, "%ld %ld", &nx, &ny);
	fgets(row, 80, inimage);

	/* allocate storage */
	alloc_matrix(&u, nx + 2, ny + 2);

	/* read image data */
	for (j = 1; j <= ny; j++)
		for (i = 1; i <= nx; i++)
			u[i][j] = (float)getc(inimage);
	fclose(inimage);


	/* ---- analyse image ---- */

	analyse(u, nx, ny, &min, &max, &mean, &vari);
	printf("\n");
	printf("minimum:       %8.2f \n", min);
	printf("maximum:       %8.2f \n", max);
	printf("mean:          %8.2f \n", mean);
	printf("std. dev.:     %8.2f \n\n", sqrt(vari));


	/* ---- read other parameters ---- */

	printf("noise standard deviation:         ");
	gets(row);  sscanf(row, "%f", &sigma);
	printf("output image:                     ");
	gets(out);
	printf("\n");


	/* ---- process image ---- */

	gauss_noise(u, nx, ny, sigma);


	/* ---- analyse processed image ---- */

	analyse(u, nx, ny, &min, &max, &mean, &vari);
	printf("minimum:       %8.2f \n", min);
	printf("maximum:       %8.2f \n", max);
	printf("mean:          %8.2f \n", mean);
	printf("std. dev.:     %8.2f \n\n", sqrt(vari));


	/* ---- write output image (pgm format P5) ---- */

	/* open file and write header (incl. filter parameters) */
	outimage = fopen(out, "wb");
	fprintf(outimage, "P5 \n");
	fprintf(outimage, "# Gaussian noise\n");
	fprintf(outimage, "# initial image:  %s\n", in);
	fprintf(outimage, "# sigma:          %8.4f\n", sigma);
	fprintf(outimage, "%ld %ld \n255\n", nx, ny);

	/* write image data and close file */
	for (j = 1; j <= ny; j++)
		for (i = 1; i <= nx; i++)
		{
			if (u[i][j] < 0.0)
				byte = (unsigned char)(0.0);
			else if (u[i][j] > 255.0)
				byte = (unsigned char)(255.0);
			else
				byte = (unsigned char)(u[i][j]);
			fwrite(&byte, sizeof(unsigned char), 1, outimage);
		}
	fclose(outimage);
	printf("output image %s successfully written\n\n", out);


	/* ---- disallocate storage ---- */

	disalloc_matrix(u, nx + 2, ny + 2);

	return(0);
}
